from django.core.management.base import BaseCommand, CommandError
from custom_command.models import  Deliveries
import csv

class Command(BaseCommand):

    # def add_arguments(self, parser):
    #     parser.add_argument('file_name', nargs='+', type=str)
    def handle(self, *args, **options):
         with open('deliveries.csv', 'r') as file:
           deliveries_reader = csv.DictReader(file)
           with transaction.atomic():
             for delivery in deliveries_reader:
                 delivery_dict = dict(delivery)
                 _, created = Deliveries.objects.update_or_create(
                 deliveries_id_id = delivery_dict['match_id'], 
                 inning = delivery_dict['inning'], 
                 batting_team = delivery_dict['batting_team'], 
                 bowling_team = delivery_dict['bowling_team'], 
                 over = delivery_dict['over'], 
                 ball = delivery_dict['ball'], 
                 batsman = delivery_dict['batsman'], 
                 non_striker = delivery_dict['non_striker'], 
                 bowler = delivery_dict['bowler'], 
                 is_super_over = delivery_dict['is_super_over'],
                 wide_runs = delivery_dict['wide_runs'], 
                 bye_runs = delivery_dict['bye_runs'], 
                 legbye_runs = delivery_dict['legbye_runs'],
                 noball_runs = delivery_dict['noball_runs'], 
                 penalty_runs = delivery_dict['penalty_runs'], 
                 batsman_runs = delivery_dict['batsman_runs'],
                 extra_runs  = delivery_dict['extra_runs'], 
                 total_runs  = delivery_dict['total_runs'],
                 player_dismissed  = delivery_dict['player_dismissed'],
                 dismissal_kind  = delivery_dict['dismissal_kind'], 
                 fielder  = delivery_dict['fielder'])