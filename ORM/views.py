from django.views.decorators.cache import cache_page
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.shortcuts import render
from django.db.models import Count, Sum, FloatField
from django.db.models.functions import Cast
from custom_command.models import Matches, Deliveries
import json


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@cache_page(CACHE_TTL)
def graph1(request):
	match_count = Matches.objects.values('season').annotate(total_match = Count('season'))
	match_count = json.dumps(list(match_count))
	context = {
			'match_count': match_count
		}
	return render(request, 'ORM/graph1.html', context)


@cache_page(CACHE_TTL)
def ipl1(request):

    match_count = Matches.objects.values('season').annotate(total_match = Count('season'))
    match_count = json.dumps(list(match_count))

    context = {

    	'match_count': match_count
    	}
    	
    return render(request, 'ORM/ipl1.html', context)


@cache_page(CACHE_TTL)
def graph2(request):
	season_wise_count = Matches.objects.values('season','winner').annotate(total_win = Count('winner'))
	season_wise_count = json.dumps(list(season_wise_count))

	context = {

    	'season_wise_count': season_wise_count
    	}

	return render(request, 'ORM/graph2.html', context)


@cache_page(CACHE_TTL)
def ipl2(request):
	season_wise_count = Matches.objects.values('season','winner').annotate(total_win = Count('winner'))
	season_wise_count = json.dumps(list(season_wise_count))

	context = {

    	'season_wise_count': season_wise_count
    	}

	return render(request, 'ORM/ipl2.html', context)


@cache_page(CACHE_TTL)
def graph3(request):
	id_2016 = Matches.objects.filter(season = '2016')
	team_given_run = Deliveries.objects.filter(deliveries_id__in = id_2016).values('bowling_team').annotate(extra_runs = Sum('extra_runs'))
	team_given_run = json.dumps(list(team_given_run))

	context = {
    	'team_given_run': team_given_run
    	}

	return render(request, 'ORM/graph3.html', context)


@cache_page(CACHE_TTL)
def ipl3(request):
	id_2016 = Matches.objects.filter(season = '2016')
	team_given_run = Deliveries.objects.filter(deliveries_id__in = id_2016).values('bowling_team').annotate(extra_runs = Sum('extra_runs'))
	team_given_run = json.dumps(list(team_given_run))

	context = {
    	'team_given_run': team_given_run
    	}

	return render(request, 'ORM/ipl3.html', context)


@cache_page(CACHE_TTL)
def graph4(request):
	id_2015 = Matches.objects.filter(season = '2015')
	eco_bowler = Deliveries.objects.filter(deliveries_id__in = id_2015).values('bowler').annotate(eco_cost = Cast(Sum('total_runs')*6.0/Count('ball'),FloatField())).order_by('eco_cost')
	eco_bowler = json.dumps(list(eco_bowler))

	context = {
    	'eco_bowler': eco_bowler
    	}

	return render(request, 'ORM/graph4.html', context)


@cache_page(CACHE_TTL)
def ipl4(request):
	id_2015 = Matches.objects.filter(season = '2015')
	eco_bowler = Deliveries.objects.filter(deliveries_id__in = id_2015).values('bowler').annotate(eco_cost = Cast(Sum('total_runs')*6.0/Count('ball'),FloatField())).order_by('eco_cost')
	eco_bowler = json.dumps(list(eco_bowler))

	context = {
    	'eco_bowler': eco_bowler
    	}

	return render(request, 'ORM/ipl4.html', context)


@cache_page(CACHE_TTL)
def graph5(request):
	id_2016 = Matches.objects.filter(season = '2016')
	best_bastman = Deliveries.objects.filter(deliveries_id__in = id_2016).values('batsman').annotate(total_run = Sum('batsman_runs')).order_by('-total_run')
	best_bastman = json.dumps(list(best_bastman))
	
	context = {
    	'best_bastman': best_bastman
    	}

	return render(request, 'ORM/graph5.html', context)


@cache_page(CACHE_TTL)
def ipl5(request):
	id_2016 = Matches.objects.filter(season = '2016')
	best_bastman = Deliveries.objects.filter(deliveries_id__in = id_2016).values('batsman').annotate(total_run = Sum('batsman_runs')).order_by('-total_run')
	best_bastman = json.dumps(list(best_bastman))
	
	context = {
    	'best_bastman': best_bastman
    	}

	return render(request, 'ORM/ipl5.html', context)